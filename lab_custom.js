

const data = {
     1: 0.25,
     2: 0.25,
     3: 0.25,
     4: 0.25,
};

let n = 1000;

function generate2(){
    let final = Object.keys(data).reduce((final, key) =>{
        final[key] = false;
        return final;
    }, {});

    let i = 0;
    while( !(Object.keys(final).every((key)=>{return final[key]})) ){
        let num = Math.random();
        let key = Object.keys(data).reduce((fnum, dkey) => {
            if(fnum) return fnum;

            if(num < data[dkey]){
                return dkey;
            }

            num -= data[dkey];

            return fnum;
        }, null);

        if(key){
            final[key] = true;
            i++;
        }
    }

    return i;
}

let sum = 0;
for(let i = 0; i < n; i++){
    sum += generate2();
}

console.log( sum / n );
console.log( Math.ceil(sum / n) );