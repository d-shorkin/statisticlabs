const jStat = require('jStat').jStat;

const x = 0.05;
const n = 10000;
const dof = 10;

function genByF(x){
    return 4 * Math.sqrt(x);
}

const data = {
     1: 0.2,
     2: 0.05,
     3: 0.05,
     4: 0.1,
     5: 0.1,
     6: 0.15,
     7: 0.13,
     8: 0.07,
     9: 0.05,
    10: 0.1
};

console.log(Object.keys(data).reduce((sum, current) => (sum += data[current]), 0));


// Генерация масссива
function generate(n) {

    let arr = [];

    for (let i = 0; i < n; i++) {
        let num = Math.random();
        let find = false;
        Object.keys(data).reduce((max, current) => {
            if(find) return 0;

            let min = max;
            max += data[current];
            if(num >= min && num < max ){
                find = true;
                num = parseInt(current);
            }
            return max;
        }, 0);
        arr[i] = num;
    }

    return arr;
}

// Подсчет вероятностей
function countProbability(arr) {
    return arr.reduce((result, current) => {
        if(typeof result[current] == 'undefined') result[current] = 0;

        result[current]++;

        return result;
    }, {});
}

// Получение критерия хи квадрат
function chiSquare(object) {
    return Object.keys(object).reduce((sum, key) => {
        let value = Math.pow((object[key] ) - ( data[key] * n ) , 2) / ( data[key] * n );
        return sum + value;
    }, 0);
}

setInterval(()=>{
    let props = countProbability(generate(n));

    console.log(props);
    console.log(chiSquare(props));

    // Получение обратного значения хи квадратё
    let hi = jStat.chisquare.cdf(chiSquare(props), Object.keys(props).length);

    console.log(hi);
    console.log(hi > x);
}, 1000);


