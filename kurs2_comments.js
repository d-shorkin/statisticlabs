const randomFloat = require('random-float');
const Prob = require('prob.js');
const randExp = Prob.exponential(.1);
// -----------------------------------------------------

// Входные параметры
const N = 1000;
let L = 10;
const S1 = .9;
const S2 = () => randomFloat(0.5,0.8);
const P = 0.3;




let residue1 = 0;
function work1(queue, time) { // Функция обработки первого прибора

    // Если еще осталось время итерация продолжается
    while (time > 0 && queue){
        time -= S1;
        if(time > 0) queue--;
    }

    // Если очередь опустела то остаток обнуляем
    residue1 = (queue) ? time : 0;
    return queue;
}

let residue2 = 0;
function work2(queue, time) {// Функция обработки второго прибора
    // Если еще осталось время итерация продолжается
    while (time > 0 && queue){
        time -= S2();
        if(time > 0 && randomFloat(0,1) > P) queue--;
    }

    // Если очередь опустела то остаток обнуляем
    residue2 = (queue) ? time : 0;
    return queue;
}


while(L < N*0.7){
    // Обнуляем все
    let queue1 = 0;
    let queue2 = 0;
    let load = [0,0];
    let totalTime = 0;

    for(let i = 0; i < N; i++){
        // Генерируем время прихода заявки
        let time = randExp();

        // Считаем общее время чтоб в последствии
        // посмотреть среднее время заявки в системе
        totalTime += time;

        // Ставим заявку в нужную очередь
        if(queue1 < L) queue1++;
        else queue2++;

        // Запускаем обработчики учитывая остатки от времени с прошлой иттерации
        residue1 = residue1 + time;
        queue1 = work1(queue1, residue1);

        residue2 = residue2 + time;
        queue2 = work2(queue2, residue2);

        // Добавляем в массивы для того чтобы потом
        // посчитать среднее количество заявок в системе
        // (загрузка приборов)
        load[1] += queue2;
        load[0] += queue1;
    }

    console.log(`${L} \t ${(load[0] / N ).toFixed(0)} \t ${(load[1] / N).toFixed(0)} \t ${totalTime / (N - queue1 - queue2)}`);
    L += 10;
}




