const randomFloat = require('random-float');
const histogram = require('ascii-histogram');
const dof = 15;

let a = .8;
let intervals = [-Math.sqrt(a), Math.sqrt(a)];

function getY(x)
{
    return a - Math.pow(x, 2);
}

let arr = [];

for (let i = 0; i < 50000; i++){
    let x = randomFloat(intervals[0], intervals[1]);
    let y = getY(x);
    if(i%2 == 0) y = -y;
    arr.push(y);
}

let result = [];
let stp = (Math.abs(intervals[0]) + intervals[1]) / dof;
let h = intervals[0];

for(let i = 0; i < dof; i++ ) {
    let value = 0;
    let sum = 0;
    for (let j = 0; j < arr.length; j++) {
        if (arr[j] > h && arr[j] <= h + stp) {
            value++;
            sum += arr[j];
        }
    }

    result.push(value);
    h += stp;

}

console.log(histogram(result));