const jStat = require('jStat').jStat;
const histogram = require('ascii-histogram');

const x = 0.05;
const n = 10000;
const dof = 10;


function genByF(y, a = 0.8){
    /*if(x < (- Math.pow(3/4, 1/6))) return 0;

    if(x >= (- Math.pow(3/4, 1/6)) && x <= (Math.pow(3/4, 1/6)))
        return Math.cbrt(Math.sqrt(3) * Math.sqrt(3 * Math.pow(x, 2) - 6 * x + 2) - 3 * x + 3) /
            Math.cbrt(2) + Math.cbrt(3/2) /  Math.cbrt(Math.sqrt(3) * Math.sqrt(3 * Math.pow(x, 2) - 6 * x + 2) - 3 * x + 3);

    if(x > (Math.pow(3/4, 1/6))) return 1;*/
/*

    return Math.cbrt(Math.sqrt(3) * Math.sqrt(3 * Math.pow(x, 2) - 6 * x + 2) - 3 * x + 3) /
        Math.cbrt(2) + Math.cbrt(3/2) /  Math.cbrt(Math.sqrt(3) * Math.sqrt(3 * Math.pow(x, 2) - 6 * x + 2) - 3 * x + 3);
*/

   // return [(0.0316 * Math.sqrt(827-1000*x)), -(0.0316 * Math.sqrt(827-1000*x))];
    return Math.pow((Math.sqrt(Math.pow(9 * y, 2) - Math.pow(12 * a, 3/2) * y) / 2 + (Math.pow(2 * a, (3/2)) - 3 * y) / 2), 1/3)
        + a / Math.pow(Math.sqrt(Math.pow(9 * y, 2) - Math.pow (12*a, 3/2) * y) / 2 + (Math.pow(2 * a, 3/2) - 3 * y) / 2, 1/3);
}


// Генерация масссива
function generate(n) {

    let arr = [];

    for (let i = 0; i < n; i++) {
        arr.push(genByF(Math.random()));
    }

    return arr;
}



// Функция расчета
function chiSquare(array, dof) {
    console.log(array);

    let result = [];
    let stp = 1 / dof;

    let h = 0;

    // Разбиение на степени свободы и подсчет их вероятностей
    for(let i = 0; i < dof; i++ ) {
        let value = 0;
        for (let j = 0; j < array.length; j++) {
            if (array[j] > h && array[j] <= h + stp) {
                value++;
            }
        }

        result.push(value);
        h += stp;

    }

    console.log(histogram(result));

    // Получение коэффициента хи квадрат
    return result.reduce((sum, current) => {
        return sum + Math.pow(current - (array.length / dof), 2) / (array.length / dof);
    }, 0);
}

setInterval(()=>{
    // Получение обратного значения хи квадратё
    let hi = jStat.chisquare.cdf(chiSquare(generate(n), dof), dof-1);

    console.log(chiSquare(generate(n), dof));

    console.log(hi);
    console.log(hi > x);
}, 1000);



