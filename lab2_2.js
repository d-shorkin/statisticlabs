const jStat = require('jStat').jStat;
const histogram = require('ascii-histogram');

const x = 0.05;
const n = 5;
const dof = 10;


function genByF(y, a = .8){
    //return 4 * Math.sqrt(x);

    y += 100;
    let res = Math.pow((Math.sqrt(Math.pow(9 * y, 2) - Math.pow(12 * a, 3/2) * y) / 2 + (Math.pow(2 * a, (3/2)) - 3 * y) / 2), 1/3)
        + a / Math.pow(Math.sqrt(Math.pow(9 * y, 2) - Math.pow (12*a, 3/2) * y) / 2 + (Math.pow(2 * a, 3/2) - 3 * y) / 2, 1/3);

    console.log(`${x}: ${res}`);

    return res;

}


// Генерация масссива
function generate(n) {

    let arr = [];

    for (let i = 0; i < n; i++) {
        arr.push(genByF(Math.random()));
    }

    return arr;
}



// Функция расчета
function chiSquare(array, dof) {
    let result = [];
    let stp = 1 / dof;
    let h = 0;

    // Разбиение на степени свободы и подсчет их вероятностей

    for(let i = 0; i < dof; i++ ) {
        let value = 0;
        for (let j = 0; j < array.length; j++) {
            if (array[j] > h && array[j] <= h + stp) {
                value++;
            }
        }
        result.push(value);

        h += stp;
    }

    console.log(histogram(result));

    // Получение коэффициента хи квадрат
    return result.reduce((sum, current) => {
        return sum + Math.pow(current - (array.length / dof), 2) / (array.length / dof);
    }, 0);
}

setInterval(()=>{
    // Получение обратного значения хи квадратё
    let hi = jStat.chisquare.cdf(chiSquare(generate(n), dof), dof-1);

    console.log(chiSquare(generate(n), dof));

    console.log(hi);
    console.log(hi > x);
}, 1000);



