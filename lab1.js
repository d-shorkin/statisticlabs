const jStat = require('jStat').jStat;

const x = 0.05;
const n = 100000;
const dof = 51;


// Генерация масссива
function generate(n) {

    let arr = [];

    for (let i = 0; i < n; i++) {
        arr.push(Math.random());
    }

    return arr;
}

// Функция расчета
function chiSquare(array, dof) {
    let result = [];
    let stp = 1 / dof;
    let h = 0;

    // Разбиение на степени свободы и подсчет их вероятностей

    for(let i = 0; i < dof; i++ ) {
        let value = 0;
        for (let j = 0; j < array.length; j++) {
            if (array[j] > h && array[j] <= h + stp) {
                value++;
            }
        }
        result.push(value);

        h += stp;
    }


    // Получение коэффициента хи квадрат
    return result.reduce((sum, current) => {
        return sum + Math.pow(current - (array.length / dof), 2) / (array.length / dof);
    }, 0);
}

setInterval(()=>{
    // Получение обратного значения хи квадратё
    let hi = jStat.chisquare.cdf(chiSquare(generate(n), dof), dof);

    console.log(hi);
    console.log(hi > x);
}, 1000);



