
let array = [];
const limit = 3;
const n = 10;

function randInt(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}


for( let i = 0; i < n; i++ ){
    array[i] = i + "n";
}

//------------------



function swap(arr, x, y){
    let tmp = arr[x];
    arr[x] = arr[y];
    arr[y] = tmp;
    return arr;
}

let limits = [];

let result = [];

for( let i = 0; i < array.length; i++ ){
    result[i] = [];
    limits[i] = 0;
    for( let j = 0; j < array.length; j++ ) result[i][j] = 0;
}

for( let i = 0; i < array.length; i++ ){
    let index = randInt(i,array.length - 1);
    array = swap(array, index, i);
    if(!i) continue;

    let root = null;

    do {
        root = randInt(0, i-1);
    } while (limits[root] >= limit);

    result[i][root] = 1;
    limits[root]++;
}

console.log(array);

result.forEach((e)=>{
    console.log(e)
});

//console.log(array);
//console.log(limits);

function generateTree(m, i = 0){
    let final = {};
    for( let j = 0; j < array.length; j++ ){
        if(m[j][i]){
            final[array[j] + ""] = generateTree(m, j);
        }
    }
    return final;
}

function renderObj(obj, lvl = 0){
    let keys = Object.keys(obj);

    if(!keys.length) return;

    for(let i = 0; i < keys.length; i++){
        let str = (keys[i] + "").padStart(5, "-----");
        for(let j = 0; j < lvl; j++){
            str = "|   " + str;
        }
        console.log(str);
        renderObj(obj[keys[i]], lvl + 1);
    }
}

final = {};
final[array[0] + ""] = generateTree(result);

console.log(JSON.stringify(final, null, 200));
//renderObj(final);
