const randomFloat = require('random-float');
const Generator = require("./kurs/Generator");
const Prob = require('prob.js');
const randExp = Prob.exponential(1);
let g = new Generator("exp", false);
// ------------------

const N = 100000;
let L = 2;
const S1 = 0.9;
const S2 = ()=>randomFloat(0.5,0.8);
const P = 0.3;
const TRANSFER = true;

let timeInQueue = 0;

function work(queue, worktime = 0, p = 0) {
    while(1){
        if(!queue.length) break;

        let r = Math.random();
        if(r < p) queue.push(queue.shift());

        if(queue[0]['value'] < worktime){
            worktime -= queue[0]['value'];
            let e = queue.shift();
            timeInQueue += (e.time + worktime);
            if(!TRANSFER) break;
        } else {
            queue[0]['value'] -= worktime;
            break;
        }
    }

    return queue;
}

function implementTime(queue) {
    return queue.map((elem)=>{
        elem.time++;
        return elem;
    });

}


let pack = 1;

setInterval(()=>{
    let queue1 = [];
    let queue2 = [];
    timeInQueue = 0;

    let load = [0,0];
    for (let i = 0; i < N; i++){
        let value = randExp();

        if(queue1.length < L){
            queue1.push({value, time:0});
        }else {
            queue2.push({value, time:0});
        }

        load[1] += Number(queue2.length > 1);
        load[0] += Number(queue1.length > 1);

        queue1 = work(queue1, S1);
        queue2 = work(queue2, S2(), P);

        queue1 = implementTime(queue1);
        queue2 = implementTime(queue2);

        //console.log(queue1.length, queue2.length);
    }


    load[0] = Number( (load[0] / N * 100).toFixed(2) );
    load[1] = Number( (load[1] / N * 100).toFixed(2) );
    console.log(`${L}\t${load[0]}\t${load[1]}\t${(timeInQueue / N).toFixed(2)}`);
    pack ++;
    L *= 2;
}, 200);




