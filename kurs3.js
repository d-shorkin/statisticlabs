const randomFloat = require('random-float');
const Generator = require("./kurs/Generator");
const Prob = require('prob.js');
const randExp = Prob.exponential(1);
let g = new Generator("exp", false);
// -----------------------------------------------------


const N = 2000;
let L = 10;
const L2 = 4;
const S1 = () => randomFloat(0.6,0.9);
const S2 = () => randomFloat(1.5,2);
const P = 0.1;


let residue1 = 0;
function work1(queue, time) {
    while (time > 0 && queue){
        time -= S1();
        if(time > 0 && randomFloat(0,1) > P) queue--;
    }

    residue1 = (queue) ? time : 0;
    return queue;
}

let residue2 = 0;
function work2(queue, time) {

    while (time > 0 && queue){
        time -= S2();
        if(time > 0) queue--;
    }

    residue2 = (queue) ? time : 0;
    return queue;
}


while(L < N*0.7){
    let queue1 = 0;
    let queue2 = 0;

    let load = [0,0];

    let totalTime = 0;

    for(let i = 0; i < N; i++){
        let time = randExp();

        totalTime += time;

        if(queue1 < L) queue1++;
        else if(queue2 < L2) queue2++;

        residue1 = residue1 + time;
        queue1 = work1(queue1, residue1);

        residue2 = residue2 + time;
        queue2 = work2(queue2, residue2);

        load[0] += queue1 ? 1 : 0;
        load[1] += queue2 ? 1 : 0;
    }

    console.log(`${L} \t ${(load[0] / N ).toFixed(2)} \t ${(load[1] / N).toFixed(2)} \t ${totalTime / (N - queue1 - queue2)}`);
    L += 10;
}




