let generator = class Generator{
    constructor(type = "line", transfer = true){
        this.type = type;
        this.transfer = transfer;

        this.queue = 0;
        this.residue = 0;
    }
    generate(){
        let value = 0;
        switch(this.type){
            case "line":
                value = Math.random();
                break;
            case "exp":
                value = Math.exp(Math.random());
                break;
            default:
                throw new Error(`Type of generator cannot be "${this.type}"`);
        }

        if(this.transfer){
            this.residue = (value + this.residue) % 1;
            value = Math.round(value - this.residue);
        }else{
            value =  Math.round(value);
        }

        return this.queue += value;
    }
    pop(){
        if(this.queue){
            return --this.queue;
        }
        throw new Error(`Can't pop from empty queue`);
    }
};

module.exports = generator;