const randomFloat = require('random-float');
const Prob = require('prob.js');
const randExp = Prob.exponential(.1);
// -----------------------------------------------------

const N = 1000;
let L = 10;
const S1 = .9;
const S2 = () => randomFloat(0.5,0.8);
const P = 0.3;


let residue1 = 0;
function work1(queue, time) {
    while (time > 0 && queue){
        time -= S1;
        if(time > 0) queue--;
    }

    residue1 = (queue) ? time : 0;
    return queue;
}

let residue2 = 0;
function work2(queue, time) {

    while (time > 0 && queue){
        time -= S2();
        if(time > 0 && randomFloat(0,1) > P) queue--;
    }

    residue2 = (queue) ? time : 0;
    return queue;
}


while(L < N*0.7){
    let queue1 = 0;
    let queue2 = 0;

    let load = [0,0];

    let totalTime = 0;

    for(let i = 0; i < N; i++){
        let time = randExp();

        totalTime += time;

        if(queue1 < L) queue1++;
        else queue2++;

        residue1 = residue1 + time;
        queue1 = work1(queue1, residue1);

        residue2 = residue2 + time;
        queue2 = work2(queue2, residue2);

        load[1] += queue2;
        load[0] += queue1;
    }

    console.log(`${L} \t ${(load[0] / N ).toFixed(0)} \t ${(load[1] / N).toFixed(0)} \t ${totalTime / (N - queue1 - queue2)}`);
    L += 10;
}




